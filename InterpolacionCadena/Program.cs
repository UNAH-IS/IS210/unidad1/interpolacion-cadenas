﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpolacionCadena
{
    class Program
    {
        static void Main(string[] args)
        {
            string cadena1;
            string cadena2;

            cadena1 = "Hola";
            cadena2 = "Mundo";

            // Es posible imprimir variables
            Console.WriteLine($"{cadena1} {cadena2}");
            // Tambien funciones
            Console.WriteLine($"{cadena1} en mayusculas: {cadena1.ToUpper()}");
            // Y expresiones propias del lenguaje
            Console.WriteLine($"3 + 4 = {3 + 4}");

            Console.ReadKey();
        }
    }
}
